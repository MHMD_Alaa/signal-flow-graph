package backEnd;

public class Edge {

	private int src;
	private int dest;
	private String gain;
	
	public Edge(int src, int dest, String gain) {
		this.src = src;
		this.dest = dest;
		this.gain = gain;
	}

	public int getSrc() {
		return src;
	}

	public int getDest() {
		return dest;
	}

	public String getGain() {
		return gain;
	}
	
}
