package backEnd;

import java.util.ArrayList;

public class SignalFlowGraph {

	private ArrayList<Edge>[] graph;
	private ArrayList<ForwardPath> forwardPaths;

	public SignalFlowGraph(int nodesNum) {
		graph = new ArrayList[nodesNum];
		forwardPaths = new ArrayList<ForwardPath>();
	}

	public void addEdge(Edge newEdge) {
		int src = newEdge.getSrc();
		int dest = newEdge.getDest();

		if (graph[src] == null) {
			graph[src] = new ArrayList<Edge>();
		}

		// check if edge is added before
		for (Edge oldEdge : graph[src]) {
			if (oldEdge.getDest() == dest) {
				graph[src].remove(oldEdge);
			}
		}

		graph[src].add(newEdge);
	}

	private void DFSForwardPaths(int src, int dest, boolean[] isVisited, ArrayList<Edge> path) {
		isVisited[src] = true;
		if (src == dest) {
			isVisited[src] = false;
			forwardPaths.add(new ForwardPath(path));
			return;
		}
		
		for (Edge edge : graph[src]) {	
			if (!isVisited[edge.getDest()]) {
				path.add(edge);
				DFSForwardPaths(edge.getDest(), dest, isVisited, path);
				path.remove(edge);
			}
		}
		isVisited[src] = false;
		
	}

}
